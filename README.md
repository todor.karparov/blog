# Simple blog service

## DB

### Diagram

![alt text](blog-db.png)

### Database used

MySQL hosted in Docker container


## API

### 1. Blog post resource

| HTTP Method | URL                                   | Status code  | Description                   |
|-------------|---------------------------------------|--------------|-------------------------------|
| GET         | /api/posts                            | 200(OK)      | Get all posts                 |
| GET         | /api/posts{id}                        | 200(OK)      | Get post by id                |
| POST        | /api/posts                            | 201(Created) | Publish post                  |
| PUT         | /api/posts/{id}                       | 200(OK)      | Update post by id             |
| DELETE      | /api/posts/{id}                       | 200(OK)      | Delete post by id             |
| GET         | /api/posts?pageSize=X&page=Y&sortBy=Z | 200(OK)      | Paginated and/or sorted posts |

### 2. Comment resource

| HTTP Method | URL                               | Status code  | Description                                              |
|-------------|-----------------------------------|--------------|----------------------------------------------------------|
| GET         | /api/posts/{postId}/comments      | 200(OK)      | Get all comments for post by id                          |
| GET         | /api/posts/{postId}/comments/{id} | 200(OK)      | Get comment by id if it belongs to post with id = postId |
| POST        | /api/posts/{postId}/comments      | 201(Created) | Publish comment for post by id                           |
| PUT         | /api/posts/{postId}/comments/{id} | 200(OK)      | Update comment by id if it belongs to post               |
| DELETE      | /api/posts/{postId}/comments/{id} | 200(OK)      | Delete comment by id if it belongs to post               |

### 3. User resource

| HTTP Method | URL                | Status code | Description |
|-------------|--------------------|-------------|-------------|
| POST        | /api/auth/register | 200(OK)     | Register    |
| POST        | /api/auth/login    | 200(OK)     | Login       |