package com.personal.project.blog.repository;

import com.personal.project.blog.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> getUserByEmail(String email);

    Optional<User> getUserById(String id);

    Optional<User> getUserByUsername(String username);

    boolean existsUserByEmail(String email);

    boolean existsUserByUsername(String username);
}
