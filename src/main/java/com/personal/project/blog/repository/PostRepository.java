package com.personal.project.blog.repository;

import com.personal.project.blog.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, String> {
    Optional<Post> getPostById(String id);

    List<Post> getPostsByCreatedBy(String userId);


}
