package com.personal.project.blog.controller;

import com.personal.project.blog.dto.LoginDto;
import com.personal.project.blog.dto.RegistrationDto;
import com.personal.project.blog.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private UserServiceImpl userService;

    @Autowired
    public AuthController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<Boolean> login(@RequestBody LoginDto login) {
        return ResponseEntity.ok(userService.login(login.getEmail(), login.getPassword()));
    }

    @PostMapping(value = "/register")
    public ResponseEntity<Void> register(@RequestBody RegistrationDto registration) {
        try {
            userService.register(registration.getUsername(), registration.getEmail(), registration.getPassword());
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().build();
    }
}
