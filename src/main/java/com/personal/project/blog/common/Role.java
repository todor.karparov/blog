package com.personal.project.blog.common;

import org.springframework.security.core.GrantedAuthority;

public record Role(Roles role) implements GrantedAuthority {

    @Override
    public String getAuthority() {
        return role.name();
    }
}
