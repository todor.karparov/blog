package com.personal.project.blog.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Post {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid4")
    private String id;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "title")
    private String title;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "last_modified")
    private LocalDate lastModified;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "content")
    @Type(type="text")
    private String content;
}
