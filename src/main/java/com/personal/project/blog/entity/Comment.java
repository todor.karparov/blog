package com.personal.project.blog.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Comment {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid4")
    private String id;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "post_id")
    private String postId;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "content")
    private String content;
}
